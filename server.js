const express = require('express');
const serveStatic = require("serve-static");
const path = require('path');
const app = express();

// create middleware to handle the serving the app
app.use("/", serveStatic (path.join (__dirname, '/dist')));

// Catch all routes and redirect to the index file
app.use('*', function (req, res) {
    res.sendFile(__dirname + '/dist/index.html')
});

app.listen(process.env.PORT || 8000);

import auth from '@/api/auth';

import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";
import Callback from '../components/Callback'

// Admin pages
const Dashboard = () => import(/* webpackChunkName: "dashboard" */ "@/pages/Dashboard.vue");
const UsersList = () => import(/* webpackChunkName: "usersList" */ "@/pages/Users/UsersList");
const UserSummary = () => import(/* webpackChunkName: "userSummary" */ "@/pages/Users/UserSummary");
const CustomersList = () => import(/* webpackChunkName: "customersList" */ "@/pages/Customers/CustomersList");
const CustomerSummary = () => import(/* webpackChunkName: "customerSummary" */ "@/pages/Customers/CustomerSummary");
const ContentsList = () => import(/* webpackChunkName: "contentsList" */ "@/pages/Contents/ContentsList");

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard,
      },
      {
        path: "users",
        name: "users",
        component: UsersList,
        beforeEnter: auth.requireAuth,
      },
      {
        path: "users/:id",
        name: "user summary",
        component: UserSummary,
        beforeEnter: auth.requireAuth,
      },
      {
        path: "customers",
        name: "customers",
        component: CustomersList,
        beforeEnter: auth.requireAuth,
      },
      {
        path: "customers/:id",
        name: "customer summary",
        component: CustomerSummary,
        beforeEnter: auth.requireAuth,
      },
      {
        path: "contents",
        name: "contents",
        component: ContentsList,
        beforeEnter: auth.requireAuth,
      },
    ]
  },
  {
    path: '/callback',
    name: "callback",
    component: Callback,
  },
  { path: "*", component: NotFound, beforeEnter: auth.requireAuth },
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;

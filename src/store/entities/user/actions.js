export default (actions, api, entity) => ({
    ...actions,
    async getLoggedByID({ commit }, id) {
        try{
            const { data: response } = await api[entity].getByID(id);
            const data = response.data;
            if(data) {
                commit('setLogged', data);
            }
        } catch (e) {
            console.error('[store][user] actions / getLoggedByID: ', e)
        }
    },
})

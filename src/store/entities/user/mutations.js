export default mutations => ({
    ...mutations,
    setLogged(state, payload) {
        state.logged = payload;
    },
})

import crud from '@/store/crud';
import api from '@/api/entities';
import userState from './state'
import userMutations from './mutations';
import userActions from './actions';

const userCrud = crud(api, 'user');

const state = userState(userCrud.state);
const actions = userActions(userCrud.actions, api,'user');
const mutations = userMutations(userCrud.mutations);

export default {
    ...userCrud,
    state,
    mutations,
    actions,
}


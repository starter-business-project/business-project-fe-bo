export default state => ({
    ...state,
    logged:{
        ...state.logged,
        id: null,
        image: null,
    },
})

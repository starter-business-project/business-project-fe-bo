import crud from '@/store/crud';
import api from '@/api/entities';
import contentState from './state';
import contentActions from './actions';

const contentCrud = crud(api, 'content');
const state = contentState(contentCrud.state);
const actions = contentActions(contentCrud.actions, api,'content');

export default {
    ...contentCrud,
    state,
    actions,
}

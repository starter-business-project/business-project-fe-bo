export default (actions, api, entity) => ({
    ...actions,
    async getManyForTable({ dispatch }, { ids, query }) {
        await dispatch('getMany', { ids, query: {...query, select: 'name description contentType url updateAt creationAt'} });
    },
    async getAllForTable({ dispatch }, query) {
        dispatch('getAll', {...query, select: 'name description contentType url updateAt creationAt'});
    },
})

import crud from '@/store/crud';
import api from '@/api/entities';

import customerState from './state';
import customerActions from './actions';

import stats from './stats';

const customerCrud = crud(api, 'customer');
const state = customerState(customerCrud.state);
const actions = customerActions(customerCrud.actions, api,'customer');

export default {
    ...customerCrud,
    state,
    actions,
    modules: {
        stats,
    }
}

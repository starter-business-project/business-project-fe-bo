import stats from '@/store/stats';
import api from '@/api/entities';
import customerStatsState from './state';

const customerStats = stats(api, 'customer');

const contentsState = customerStatsState(customerStats.state);

export default {
    ...customerStats,
    state: {
        contents: contentsState
    },
}


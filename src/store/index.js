import Vue from 'vue'
import Vuex from 'vuex'
import user from '@/store/entities/user'
import customer from '@/store/entities/customer'
import content from '@/store/entities/content'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        user,
        customer,
        content,
    },
    strict: debug,
})

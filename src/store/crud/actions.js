import { getFilter } from '../../helpers/convertData'

export default (api, entity) => ({
    async getAll({ commit }, query) {
        const { data: response } = await api[entity].getAll(query);
        if(response && response.data) {
            commit('setAll', response.data);
            const filter = getFilter(query);
            const filterValue = filter && Object.values(filter)[0];
            commit('setFilter', filterValue);

        }
    },
    async getAllForTable({ dispatch }, query) {
        dispatch('getAll', {...query, select: 'firstName lastName email status updateAt creationAt'});
    },
    async getMany({ commit, state }, { ids, query }) {
        try {
            const { data: response } = await api[entity].getMany(ids, query);
            if(response && response.data) {
                let data = response.data;
                commit('setAll', data);
                const filter = getFilter(query);
                const filterValue = filter && Object.values(filter)[0];
                commit('setFilter', filterValue);
            }
        } catch (e) {
            console.error('[store] actions / getMany: ', e)
        }
    },
    async getManyForTable({ dispatch }, { ids, query }) {
        await dispatch('getMany', { ids, query: {...query, select: 'firstName lastName email status updateAt creationAt'} });
    },
    async getAllByOidBySelected({ dispatch, rootState }, query) {
        const entitySelected = query && query.entity;
        const collection = query && query.collection;
        delete query.entity;
        delete query.collection;

        const objectId = rootState && rootState[entitySelected] && rootState[entitySelected].selected && rootState[entitySelected].selected._id;
        dispatch('getAll', {...query, [collection]: objectId, select: 'name description contentType url updateAt creationAt' });
    },
    async getListWithOidBySelected({ commit, dispatch, rootState }, query) {
        const entitySelected = query && query.entity;
        const collection = query && query.collection;
        delete query.entity;
        delete query.collection;
        const id = rootState && rootState[entitySelected] && rootState[entitySelected].selected && rootState[entitySelected].selected._id;

        const { data: response } = await api[entity].getListByOid(collection, id, query);
        if(response && response.data) {
            commit('setAll', response.data);
            const filter = getFilter(query);
            const filterValue = filter && Object.values(filter)[0];
            commit('setFilter', filterValue);
        }
    },
    async getListWithOidBySelectedForTable({ commit, dispatch, rootState }, query) {
        dispatch('getListWithOidBySelected', {...query, select: 'firstName lastName email status updateAt creationAt'});
    },
    async getByID({ commit }, id) {
        const { data: response } = await api[entity].getByID(id);
        await commit('setSelected', response.data);
    },
    clearSelected({ commit }) {
        commit('clearSelected');
    },
    clearAll({ commit }) {
        commit('setAll', {
            docs: [],
            hasNextPage: false,
            hasPrevPage: false,
            limit: 10,
            nextPage: null,
            offset: 0,
            page: 1,
            pagingCounter: 1,
            prevPage: null,
            totalDocs: 0,
            totalPages: 1,
        });
    },
    async update({ dispatch, commit }, payload) {
        const { status, data: { success } } = await api[entity].update(payload);
        await commit('isUpdated', status === 200 && success);
        if(status === 200 && success) {
            await dispatch('getByID', payload._id);
        }
    },
    async add({ dispatch, commit }, payload) {
        const { status, data: { success, data: { _id } } } = await api[entity].add(payload);
        await commit('isAdded', status === 200 && success);
        if(status === 200 && success && _id) {
            await dispatch('getByID', _id);
        }
    },
    async deleteById({ dispatch, commit }, id) {
        const { data: { success } } = await api[entity].deleteById(id);
        await commit('isDeleted', success);
        if(success) {
            await dispatch('getAllForTable');
        }
    },
});

import stateMock from "./mocks/state.mock";
import getters from '../crud/getters';
import moment from "moment";

describe('CRUD / getters', () => {

    describe('itemsTable', () => {
        it('get all users data formatted', async () => {
            const result = getters.itemsTable(stateMock);
            const expected = stateMock.table.docs && stateMock.table.docs.map(user => {
                return {
                    ...user,
                    creationAt: moment(user.creationAt).format("MM/DD/YYYY, HH:mm"),
                    updateAt: moment(user.updateAt).format("MM/DD/YYYY, HH:mm"),
                }
            });
            expect(expected).toEqual(result);
        });
    });

});

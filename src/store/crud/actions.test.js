import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'
import api from '../../api/entities';
import apiMock from './mocks/api.mock';
import crud from './';
import stateMock from './mocks/state.mock';

jest.mock('../../api/entities');

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
    modules: { user: {...crud(api, 'user')} }
});

describe('CRUD / actions', () => {
    const payload = {
        connectionType: "social",
        creationAt: "2019-07-11T12:43:57.000Z",
        email: "john.doe@mail.com",
        firstName: "John",
        isAuthorized: false,
        lastName: "Doe",
        password: "$2b$10$QOJ/zyEiVbJS1B1ANR//C.aS3DQLgMq1hVkWqwxUl6ZYPSgoViTiC",
        roles: ["guest", "staff"],
        status: "pending",
        updateAt: "2019-08-12T07:45:40.307Z",
        userName: "johnDoe",
        _id: "id-1",
    };

    describe('getAll', () => {
        it('get all users data', async () => {

            expect(store.state.user.table.docs).toEqual([]);
            api.user.getAll.mockResolvedValue(apiMock.getAll());
            await store.dispatch('user/getAll');
            expect(store.state.user.table.docs).toEqual(stateMock.table.docs);
            expect(store.state.user.table.totalDocs).toEqual(stateMock.table.totalDocs);
        });
    });

    describe('getByID', () => {
        it('get user data by id', async () => {

            expect(store.state.user.selected).toEqual(null);
            api.user.getByID.mockResolvedValue(apiMock.getByID(stateMock.selected._id));
            await store.dispatch('user/getByID');
            expect(store.state.user.selected).toEqual(stateMock.selected);
        });
    });

    describe('clearSelected', () => {
        it('clear user selected', async () => {

            api.user.getByID.mockResolvedValue(apiMock.getByID(stateMock.selected._id));
            await store.dispatch('user/getByID');
            expect(store.state.user.selected).toEqual(stateMock.selected);

            await store.dispatch('user/clearSelected');
            expect(store.state.user.selected).toEqual(null);
        });
    });

    describe('update', () => {
        it('notification update is true', async () => {
            api.user.update.mockResolvedValue(apiMock.update(payload));
            await store.dispatch('user/update', payload);
            expect(store.state.user.notifications.updated).toEqual(true);
        });
    });

    describe('add', () => {
        it('notification add is true', async () => {
            api.user.add.mockResolvedValue(apiMock.add(payload));
            await store.dispatch('user/add', payload);
            expect(store.state.user.notifications.added).toEqual(true);
        });
    });

    describe('deleteById', () => {
        it('notification deleted is true', async () => {
            api.user.deleteById.mockResolvedValue(apiMock.deleteById(stateMock.selected._id));
            await store.dispatch('user/deleteById', stateMock.selected._id);
            expect(store.state.user.notifications.deleted).toEqual(true);
        });
    });
});

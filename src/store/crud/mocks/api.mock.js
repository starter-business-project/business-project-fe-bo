import response from './state.mock';

export default {
    getAll: () => ({
        data: {
            data: response.table,
        },
        success: true,
    }),

    getByID: id => ({
        data: {
            data: response.table.docs.find(user => user._id === id),
        },
        success: true,
    }),

    update: payload => ({
        data: {
            data: null,
            success: true,
        },
        status: 200,
    }),

    add: payload => ({
        data: {
            data: payload,
            success: true,
        },
        status: 200,
    }),

    deleteById: id => ({
        data: {
            success: true,
        },
    }),
}

export default {
    setAll(state, payload) {
        state.table = payload;
    },
    setSelected(state, payload) {
        state.selected = payload;
    },
    clearSelected(state) {
        state.selected = null;
        state.notifications = {
            updated: false,
            added: false,
            deleted: false,
        };
    },
    isUpdated(state, payload) {
        state.notifications.updated = payload;
    },
    isAdded(state, payload) {
        state.notifications.added = payload;
    },
    isDeleted(state, payload) {
        state.notifications.deleted = payload;
    },
    setFilter(state, payload) {
        state.filter = {
            value: payload,
            active: !!payload
        }
    },
    pushSublistIds(state, { sublist, id }) {
        state.selected && state.selected[sublist] && state.selected[sublist].push(id);
    },
    deleteSubListIds(state, { sublist, id }) {
        if(state.selected && state.selected[sublist] && state.selected[sublist].length > 0) {
            const index = state.selected[sublist].indexOf(id);
            state.selected[sublist].splice(index, 1);
        }
    },
};

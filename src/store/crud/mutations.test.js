import mutations from './mutations';
import state from './state';
import payload from './mocks/state.mock';

describe('CRUD / mutations', () => {
    describe('setAll', () => {
        it('get all users data mutated', async () => {

            expect(state.table.docs).toEqual([]);
            expect(state.table.totalDocs).toEqual(0);
            mutations.setAll(state, payload.table);
            expect(state.table.docs).toEqual(payload.table.docs);
            expect(state.table.totalDocs).toEqual(payload.table.docs.length);
        });
    });

    describe('setSelected', () => {
        it('get user selected data mutated', async () => {

            expect(state.selected).toEqual(null);
            mutations.setSelected(state, payload.selected);
            expect(state.selected).toEqual(payload.selected);
        });
    });

    describe('clearSelected', () => {
        it('get all notifications data and user selected data mutated', async () => {

            mutations.setSelected(state, payload.selected);
            mutations.isUpdated(state, true);
            mutations.isAdded(state, false);
            mutations.isDeleted(state, true);
            expect(state.selected).toEqual(payload.selected);
            expect(state.notifications).toEqual({
                updated: true,
                added: false,
                deleted: true,
            });

            mutations.clearSelected(state);
            expect(state.selected).toEqual(null);
            expect(state.notifications).toEqual({
                updated: false,
                added: false,
                deleted: false,
            });
        });
    });

    describe('isUpdated', () => {
        it('get updated attribute mutated', async () => {
            expect(state.notifications.updated).toEqual(false);
            mutations.isUpdated(state, true);
            expect(state.notifications.updated).toEqual(true);
        });
    });

    describe('isAdded', () => {
        it('get added attribute mutated', async () => {
            expect(state.notifications.added).toEqual(false);
            mutations.isAdded(state, true);
            expect(state.notifications.added).toEqual(true);
        });
    });

    describe('isDeleted', () => {
        it('get deleted attribute mutated', async () => {
            expect(state.notifications.deleted).toEqual(false);
            mutations.isDeleted(state, true);
            expect(state.notifications.deleted).toEqual(true);
        });
    });
});

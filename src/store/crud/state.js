export default {
    table: {
        docs: [],
        hasNextPage: false,
        hasPrevPage: false,
        limit: 10,
        nextPage: null,
        offset: 0,
        page: 1,
        pagingCounter: 1,
        prevPage: null,
        totalDocs: 0,
        totalPages: 1,
    },
    selected: null,
    notifications: {
        updated: false,
        added: false,
        deleted: false,
    },
    filter: {
        value: null,
        active: false
    },
}

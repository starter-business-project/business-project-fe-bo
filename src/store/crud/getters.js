import moment from 'moment';

export default {
    itemsTable: state => {
        const docs = state && state.table && state.table.docs;
        return docs && docs.length > 0 ? docs.map(doc => {
            return {
                ...doc,
                creationAt: moment(doc.creationAt).format("MM/DD/YYYY, HH:mm"),
                updateAt: moment(doc.updateAt).format("MM/DD/YYYY, HH:mm"),
            }
        }) : [];
    },
    paginationTable: state => {
        const pagination = {...state.table};
        delete pagination.docs;
        return pagination;
    },
    queryPagination: state => {
        const { page, limit } = state.table;
        return { page, limit };
    },
}

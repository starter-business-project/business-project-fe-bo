export default {
    setSublistStats(state, { sublistName, payload }) {
        state[sublistName] = payload;
    },
};

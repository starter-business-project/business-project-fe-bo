import { payloadForChart } from "@/helpers/convertData";

export default (api, entity) => ({
    async getStatsSublist({ commit }, sublistName) {
        const { data: response } = await api[entity].getStatsSublist(sublistName);
        if(response && response.data && response.data.length > 0) {
            const data = payloadForChart(response.data[0]);
            commit('setSublistStats', { sublistName, payload: data });
        }
    },
});

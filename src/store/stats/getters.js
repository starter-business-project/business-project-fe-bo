export default {
    getChartData: state => sublistName => {
        if(state && state[sublistName]) {
            const labels = Object.keys(state[sublistName]);
            const data = Object.values(state[sublistName]);
            return { labels, data }
        }
        return { labels: [], data: [] }
    },
}

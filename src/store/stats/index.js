import actions from './actions'
import state from './state'
import getters from './getters'
import mutations from './mutations'

export default (api, entity) => ({
    namespaced: true,
    state,
    actions: actions(api, entity),
    getters,
    mutations,
})

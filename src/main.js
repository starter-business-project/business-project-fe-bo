import Vue from "vue";
import VueRouter from "vue-router";
import RouterPrefetch from 'vue-router-prefetch'
import App from "./App";
import router from "./router/index";

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import BlackDashboard from "./plugins/blackDashboard";
import AuthPlugin from "./plugins/authPlugin";
import locales from "./locales";
import './registerServiceWorker';
import store from '@/store';
import VueMoment from 'vue-moment';
import VuePlyr from 'vue-plyr'

const opts = {
  dark: true,
  icons: {
    iconfont: 'fa4'
  },
};

Vue.use(Vuetify);
Vue.use(AuthPlugin);
Vue.use(BlackDashboard);
Vue.use(VueRouter);
Vue.use(RouterPrefetch);
Vue.use(VueMoment);
Vue.use(VuePlyr, {
  plyr: {
    fullscreen: { enabled: false }
  },
  emit: ['ended']
});

/* eslint-disable no-new */
new Vue({
  vuetify: new Vuetify(opts),
  router,
  i18n: locales,
  store,
  render: h => h(App)
}).$mount("#app");

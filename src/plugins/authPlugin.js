import auth from '@/api/auth';

export default {
    install(Vue) {
        Vue.prototype.$auth = auth;
    }
}

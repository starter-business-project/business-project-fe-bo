export const CONNECTION_TYPE = [
    'default',
    'social',
];

export const STATUS = [
    'pending',
    'archived',
    'suspended',
    'activated',
];

export const ROLES = [
    'admin',
    'staff',
    'customer',
    'guest',
];

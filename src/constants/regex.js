export const charNumberRegex = /^[a-zA-Z0-9]+$/;

export const numberRegex = /^[0-9]+$/;

export const emailRegex = /.+@.+\..+/;

export const charAccentsCharRegex = /^[a-zA-ZÀ-ÿ]+$/;

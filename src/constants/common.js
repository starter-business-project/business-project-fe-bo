export const COLUMNS_LIST = ["firstName", "lastName", "email", "status", "updateAt", "creationAt"];
export const FILTERS = ["", "firstName", "lastName", "email", "status"];
export const ENTITIES = {
    user: 'users',
    content: 'contents',
    customer: 'customers',
};
export const ENTITY = {
    users: 'user',
    contents: 'content',
    customer: 'customer',
};

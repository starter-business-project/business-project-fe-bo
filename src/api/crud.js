import api from './index';
import querystring from 'qs';

export default url => ({
    getAll: query => api.get(`${url}?${querystring.stringify(query)}`),

    getByID: id => api.get(`${url}/${id}`),

    getListByOid: (collection, id, query) => api.get(`${url}/collections/${collection}/${id}?${querystring.stringify(query)}`),

    update: item => api.put(`${url}/${item._id}`, item),

    add: item => api.post(url, item),

    deleteById: id => api.delete(`${url}/${id}`),

    getMany: (ids, query) => api.post(`${url}/many?${querystring.stringify(query)}`, ids),
})

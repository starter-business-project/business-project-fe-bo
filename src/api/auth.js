import Router from 'vue-router';
import axios from "axios";
import * as jwt from 'jsonwebtoken';
import moment from "moment";

const X_API_KEY = process.env.VUE_APP_X_API_KEY;
const ACCESS_TOKEN_KEY = 'access_token';

export default class Auth {
    static router = new Router({
        mode: 'history',
    });

    static checkExpirationToken = () => {
        const accessToken = JSON.parse(localStorage.getItem(ACCESS_TOKEN_KEY));
        const token = accessToken && accessToken.token;
        if(token) {
            const { exp } = jwt.decode(token);
            if(moment() > moment.unix(exp)) {
                this.logout();
            }
        }
    };

    static setAccessToken = ({token, userId}) => {
        localStorage.setItem(ACCESS_TOKEN_KEY, JSON.stringify({ token, userId }));
        sessionStorage.setItem(ACCESS_TOKEN_KEY, JSON.stringify({ token, userId }));
    };

    static getAccessToken = () => {
        this.checkExpirationToken();
        return JSON.parse(localStorage.getItem(ACCESS_TOKEN_KEY));
    };

    static clearAccessToken = () => {
        localStorage.removeItem(ACCESS_TOKEN_KEY);
        sessionStorage.removeItem(ACCESS_TOKEN_KEY)
    };

    static getApiKey = () => X_API_KEY;

    // DEPRECATED
    static loginGoogle = () => {
        return null;
    };

    // DEPRECATED
    static loginGitHub = () => {
        return null;
    };

    static loginDefaultCredentials = async (email, password) => {
        const { data } = await axios.post(`${process.env.VUE_APP_URL_API_PROD}/auth/login`, {email, password}, { headers: {
            'X-API-key': this.getApiKey(),
        }});
        if(data.success) {
            this.setAccessToken(data);
            this.router.go('./');
        } else {
            return data.message
        }
    };

    static logout = () => {
        this.clearAccessToken();
        this.router.go('./');
    };

    static isLoggedIn = () => {
        const accessToken = this.getAccessToken();
        const token = accessToken && accessToken.token;
        return !!token;
    };

    static requireAuth = (to, from, next) => {
        const isLogged = this.isLoggedIn();
        if (!isLogged) {
            next({
                path: 'dashboard',
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    }
}



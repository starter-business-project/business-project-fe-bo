import api from './index';

export default entity => {
    const url = `/stats${entity}`;

    return {
        getStatsSublist: sublistName => api.get(`${url}/sublist/${sublistName}`),
    }
}

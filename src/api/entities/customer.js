import crud from '../crud';
import stats from '../stats';

const apiCrud = crud('/customers');
const apiStats = stats('/customers');

export default {
    ...apiCrud,
    ...apiStats,
}

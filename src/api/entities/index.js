import user from './user';
import customer from './customer';
import content from './content';

export default {
    user,
    customer,
    content,
}

import crud from '../crud';

const apiCrud = crud('/contents');

export default {...apiCrud}

import auth from './auth';
import axios from 'axios';

const token = auth.getAccessToken() && auth.getAccessToken().token;

export default class Api {
    static instance = axios.create({
        baseURL: process.env.VUE_APP_URL_API_PROD,
        timeout: 10000,
        headers: {
            Authorization: `Bearer ${token}`,
            'X-API-key': auth.getApiKey(),
        }
    });

    static get = url => this.instance.get(url);

    static post = (url, payload) => this.instance.post(url, payload);

    static put = (url, payload) => this.instance.put(url, payload);

    static delete = url => this.instance.delete(url);
}

import moment from "moment";

export default payload => ({
    ...payload,
    birthDate: payload && payload.birthDate && moment(payload.birthDate).format("YYYY-MM-DD"),
    address: payload && payload.address || {
        street: null,
        zipCode: null,
        city: null,
        country: null,
    },
    image: payload && payload.image,
    email: payload && payload.email,
    isAuthorized: payload && payload.isAuthorized || false,
    password: payload && payload.password,
    roles: payload && payload.roles || [],
    status: payload && payload.status,
    firstName: payload && payload.firstName,
    lastName: payload && payload.lastName,
    userName: payload && payload.userName || '',
    phone: payload && payload.phone,
    description: payload && payload.description,
})

export default payload => ({
    ...payload,
    name: payload && payload.name,
    description: payload && payload.description,
    url: payload && payload.url,
    videoType: payload && payload.videoType,
    contentType: payload && payload.contentType,
});

import User from "./user";

export default payload => ({
    ...User(payload),
    monitoring: payload && payload.monitoring || [],
});

export const fileToBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

export const getFilter = query => {
    if(query) {
        delete query.page;
        delete query.limit;
        delete query.select;
    }
    return query
}

export const payloadForChart = payload => {
    if(payload) {
        delete payload._id;
    }
    return payload
};

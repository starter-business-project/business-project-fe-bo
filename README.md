# Business Project / FE / BO

Web Application with Vue.js and Vuex

## Requirements

[NodeJS](https://nodejs.org/en/)

## Getting Started


Then install the dependencies

```
npm install
```

## Start the server

Run in development mode

```
npm run serve
```

Run tests

```
npm run test
```

## Project architecture

API, FO and BO Hosted with Heroku in CI process.

#### Database
- Description: Database cloud-hosted
- Main libraries: MongoDB Atlas (Cloud)

#### API
- URL: https://business-project-be-api.herokuapp.com
- Gitlab : https://gitlab.com/starter-business-project/business-project-be-api
- Description: API REST
- Main libraries: Typegoose / Mongoose, Express.js

#### Front-Office (FO)
- URL: http://business-project-fe-fo.herokuapp.com
- Gitlab : https://gitlab.com/starter-business-project/business-projet-fe-fo
- Description: For administer the data. And handle accounts granting
- Main libraries: Vue.js, Vuetify, Vuex

#### Back-Office (BO)
- URL: http://business-project-fe-bo.herokuapp.com
- Gitlab : https://gitlab.com/starter-business-project/business-project-fe-bo
- Description: Especially application for customers.
- Main libraries: Vue.js, Vuetify, Vuex

#### business-project-vue-library
- Gitlab : https://gitlab.com/starter-business-project/business-projet-library-vue
- Description: Components library shared with FO and BO. It installed with NPM from Gitlab.
- Main libraries: Vue.js, Vuetify, Storybook (Test components)


![alt project architecture](src/assets/arch-business_project.png)

## Documentation of template (Vue Black Dashboard)
The documentation for the Vue Black Dashboard is hosted at our [website](https://demos.creative-tim.com/vue-black-dashboard/documentation).
